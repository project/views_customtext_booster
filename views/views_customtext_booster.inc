<?php

/**
 * @file
 * Definition of views_customtext_booster, based on views_handler_field_custom.
 */

/**
 * A handler to provide a field that is completely custom by the administrator, with filter support.
 *
 * @ingroup views_field_handlers
 */
class views_customtext_booster extends views_handler_field {
  function query() {
    // do nothing -- to override the parent query.
  }

  function option_definition() {
    $options = parent::option_definition();

    // Override the alter text option to always alter the text.
    $options['alter']['contains']['alter_text'] = array('default' => TRUE, 'bool' => TRUE);
    $options['hide_alter_empty'] = array('default' => FALSE, 'bool' => TRUE);
    $options['usetokens'] = array('default' => '', 'bool' => TRUE);
    return $options;
  }

  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    // Alter the textarea.
    $form['alter']['text']['#type'] = 'text_format';

    // If we are using a format, default value is an array with both the text and used format.
    if (is_array($form['alter']['text']['#default_value'])) {
      $form['alter']['text']['#format'] = $form['alter']['text']['#default_value']['format'];
      $form['alter']['text']['#default_value'] = $form['alter']['text']['#default_value']['value'];

    } 
    else {
      $form['alter']['text']['#format'] = NULL;

    }

    $form['alter']['text']['#weight'] = -2;

    // Apply token replacement
    $form['usetokens'] = array(
      '#type'          => 'checkbox',
      '#title'         => 'Replace tokens',
      '#default_value' => $this->options['usetokens'],
      '#weight'        => -1
    );

    // Remove the checkbox
    unset($form['alter']['alter_text']);
    unset($form['alter']['text']['#dependency']);
    unset($form['alter']['text']['#process']);
    unset($form['alter']['help']['#dependency']);
    unset($form['alter']['help']['#process']);
    $form['#pre_render'][] = 'views_handler_field_custom_pre_render_move_text';
  }

  function render($values) {
    // Normalize text and format input.
    if (is_array($this->options['alter']['text'])) {
      $text = $this->options['alter']['text']['value'];
      $format = $this->options['alter']['text']['format'];
    }
    else {
      $text = $this->options['alter']['text'];
      $format = NULL;
    }

    // First apply token replacement if needed.
    if ($this->options['usetokens'] === 1) {
      $options = array();

      // Add more context?
      if (isset($values->nid)) { 
        $options['node'] = node_load($values->nid); 
      }

      $text = token_replace($text, $options);
    }

    // If requested, apply a format.
    if (!is_null($format)) {
      $text = check_markup($text, $format, TRUE);
    } 

    // And apply the finalized text.
    $this->options['alter']['text'] = $text;
    
    // Return the text, so the code never thinks the value is empty.
    return $text;    
  }
}

/**
 * Prerender function to move the textarea to the top.
 */
function views_handler_field_custom_pre_render_move_text($form) {
  $form['text'] = $form['alter']['text'];
  $form['help'] = $form['alter']['help'];
  unset($form['alter']['text']);
  unset($form['alter']['help']);

  return $form;
}
