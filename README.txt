###INTRODUCTION

The Views Custom Text Booster module gives new options for the "Global: Custom Text" 
field included in Views, namely the application of text formats and token replacement.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/views_customtext_booster

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/views_customtext_booster

### What does this module do?

The field "plain" text area is replaced with a "text-format-enabled version of a 
textarea" ( Drupal FormAPI text_format ). This means that individual text formats 
(and associated filters) can be applied to the contents of the given text. 

In addition a checkbox allows token replacement to be enabled. If the View is Node 
based, the current row node is passed as context for token replacement.

### Module requirements

 * Views

### How to use the module?

 * Enable as per normal Drupal module installation.
 * Create a view
 * Add a "Global: Custom Text" field to the view.
 * In the configuration dialog of the field, you may now use the features of this module.

### Is there a D8 version of this on the way?

Yup.
